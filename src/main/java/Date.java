public class Date {


    //definir les attributs

    public int jour;
    public int mois;
    public int an;

    private static String[] moisLettres = {"janvier","fevrier","mars","avril", "mai" , "juin" , "juillet" , "aout", "septembre", "octobre", "novembre" , "decembre"};


    public Date(int jour, int mois, int an) {
        this.jour = jour;
        this.mois = mois;
        this.an = an;
    }
    public Date(Date d){
        this(d.jour, d.mois, d.an);
    }


    public String toString() {
        return this.jour + " " + moisLettres[this.mois -1] + " " + this.an;
    }

    private boolean anneeEstBissextile(){
        return (this.an % 4 == 0 && this.an % 100 != 0) || (this.an % 400 == 0);
    }

    public int nbJoursMois () {
        int[] jourParMois = {31,28,31,30,31,30,31,31,30,31};
        if (this.mois == 2 && this.anneeEstBissextile()){
            return 29;
        }
        return jourParMois[this.mois-1];
    }

    public void incrementer(){
        if (this.jour < nbJoursMois()){
            this.jour++;
        } else {
            this.jour = 1;
            if (this.mois < 12){
                this.mois++;
            }else {
                this.mois = 1;
                this.an++;
            }
        }
    }

    public Date lendemain () {
        Date nDate = new Date(this);
        nDate.incrementer();
        return nDate;
    }

    public boolean egale (Date d){
        return (this.mois == d.jour && this.mois == d.mois && this.an == d.an);
    }

    public boolean anterieure (Date d){
        if (this.an < d.an){
            return true;
        }
        if (this.an == d.an && this.mois < d.mois){
            return true;
        }
        return this.an == d.an && this.mois == d.an && this.jour < d.jour;
    }
    public boolean posterieure (Date d){
        return !this.anterieure(d) && !this.egale(d);
    }

    public int ecart (Date d) {
        Date sDate = this.anterieure(d) ? this : d;
        Date eDate = this.anterieure(d) ? d : this;
        int dDate = 0;
        while (!sDate.egale(eDate)){
            sDate.incrementer();
            dDate++;
        }
        return dDate;
    }


}
