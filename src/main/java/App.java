public class App {

    public static void main(String args[]){
        //ecrire vos tests ici
        ///Fraction f1, f2, f3, f4; // declaration de 4 variables/instances de type/classe Fraction

	///f1 = new Fraction(-4,5); // fabrication d'une instance de la classe Fraction à l'aide du constructeur a deux arguments entiers

	///f2 = new Fraction(13,26); // fabrication d'une instance de la classe Fraction à l'aide du constructeur a deux arguments entiers

	///f3 = new Fraction("24/36"); // fabrication d'une instance de la classe Fraction à partir d'une chaîne de caractères

	///f4 = new Fraction(f2); // fabrication d'une instance de la classe Fraction par recopie de l'instance f2



	///System.out.print ("f1 = "); System.out.println (f1.toString());
	///System.out.println ("f2 = " + f2.toString());
	///System.out.println ("f3 = " + f3.toString());
	///System.out.println ("f4 = " + f4); // f4 n'etant pas de type String, f4.toString() est invoquee automatiquement : le resultat est concatene a la chaine precedente ("f4 = ")

		///Fraction puissance = f4.puissance(1);
		///System.out.println (puissance.toString());
		Date date1 = new Date(14, 3, 2020);
		Date date2 = new Date(15, 3, 2020);

		System.out.println("Date initiale : " + date1); // 14 mars 2020
		date1.incrementer();
		System.out.println("Date après incrément : " + date1); // 15 mars 2020

		Date lendemain = date1.lendemain();
		System.out.println("Lendemain de " + date1 + " : " + lendemain); // 16 mars 2020

		System.out.println("Les deux dates sont-elles égales ? " + date1.egale(date2)); // true
		System.out.println("La date 1 est-elle antérieure à la date 2 ? " + date1.anterieure(date2)); // false
		System.out.println("La date 1 est-elle postérieure à la date 2 ? " + date1.posterieure(date2)); // false

		Date date3 = new Date(1, 1, 2020);
		System.out.println("Nombre de jours entre " + date3 + " et " + date2 + " : " + date3.ecart(date2));
	}
}
